# Mario50-pixi

1. Run the respective build script (default build task is preconfigured for VS Code), *OR*
    1. Run `npm install`.
    1. Run `rollup -cw` to incrementally bundle your source code.
1. Serve the app using your choice of server application.
