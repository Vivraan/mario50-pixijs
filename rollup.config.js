import commonjs from '@rollup/plugin-commonjs';
import html from '@rollup/plugin-html'
import { nodeResolve } from '@rollup/plugin-node-resolve';
import pluginJSON from '@rollup/plugin-json'
import { terser } from 'rollup-plugin-terser'

export default {
    input: 'main.js',
    output: [
        {
            dir: './build/',
            entryFileNames: 'bundle.[hash].js',
            chunkFileNames: 'bundle.[hash].js',
            format: 'esm',
        },
        // {
        //     dir: './build/',
        //     entryFileNames: 'bundle.[hash].min.js',
        //     chunkFileNames: 'bundle.[hash].min.js',
        //     format: 'esm',
        //     plugins: [terser()]
        // },
    ],
    plugins: [
        commonjs(),
        html(),
        nodeResolve({
            preferBuiltins: false
        }),
        pluginJSON(),
    ]
}
