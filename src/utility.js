import { Ticker } from 'pixi.js'
import CustomInputMap from './custom_input_map.json'

export const INPUT_MOVE_LEFT = CustomInputMap.LEFT ?? 'KeyA'
export const INPUT_MOVE_RIGHT = CustomInputMap.RIGHT ?? 'KeyD'
export const INPUT_JUMP = CustomInputMap.JUMP ?? 'Space'

export const TITLE = 'This is Mario50.'

/** The inverse or reciprocal of the scale factor. Use this when you need to logically divide a value by the scale factor. */
export const INV_SCALE_FACTOR = 27 / 80
/** The constant by which the game screen is scaled. */
export const SCALE_FACTOR = 1 / INV_SCALE_FACTOR

export const SCREEN_WIDTH = 1280
export const SCREEN_HEIGHT = 720

/** The screen width scaled by the scale factor. */
export const VIRTUAL_WIDTH = SCREEN_HEIGHT * INV_SCALE_FACTOR
/** The screen height scaled by the scale factor. */
export const VIRTUAL_HEIGHT = SCREEN_HEIGHT * INV_SCALE_FACTOR

export const FONT_PATH = 'fonts/04b03.ttf'

/**
 * Returns delta time in seconds.
 * @param {Ticker} ticker Ticker to query deltaMS from
 */
export function deltatime(ticker) {
    return ticker.deltaMS * 0.001
}
