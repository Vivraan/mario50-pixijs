import { Application, settings, SCALE_MODES, utils, Loader } from 'pixi.js'
import { Camera } from './camera'
import { GameObject } from './game_object'
import { LOAD_PARAMS as PLAYER_PARAMS, Player } from './player'
import { LOAD_PARAMS as TILEMAP_PARAMS, Tilemap } from './tilemap'
import { SCALE_FACTOR, SCREEN_HEIGHT, SCREEN_WIDTH, TITLE } from './utility'

const Colors = {
    SKY_BLUE: utils.rgb2hex([108 / 255, 140 / 255, 1])
}

const GameState = { PLAY: 0, END: 1 }

// let gameState
// const music

// register()

document.title = TITLE

// Set default scaling mode to nearest neighbours algorithm
settings.SCALE_MODE = SCALE_MODES.NEAREST

// The application will create a renderer using WebGL, if possible,
// with a fallback to a canvas render. It will also setup the ticker
// and the root stage Container
const app = new Application({
    width: SCREEN_WIDTH,
    height: SCREEN_HEIGHT,
    autoDensity: true,
    autoDensity: true,
    backgroundColor: Colors.SKY_BLUE,
});

app.view.style.position = 'absolute'
app.view.style.left = '16.67%'
app.view.style.top = '12.5%'

// The application will create a canvas element for you that you
// can then insert into the DOM
document.body.appendChild(app.view);

app.stage.scale.set(SCALE_FACTOR)

const tilemap = new Tilemap(app)
const player = new Player(app)

Loader.shared
    .add(...TILEMAP_PARAMS)
    .add(...PLAYER_PARAMS)
    .load((_loader, resources) => {
        tilemap.load(_loader, resources)
        player.load(_loader, resources)
        new Camera(app, player)
    })

window.addEventListener('unload', GameObject.cleanupObjects)
