import { Application, ObservablePoint } from 'pixi.js'
import { GameObject } from './game_object';
import { MAP_WIDTH } from './tilemap';
import { VIRTUAL_WIDTH } from './utility';

export class Camera extends GameObject {
    x = 0

    /**
     * A camera that follows a target.
     * @param {Application} app
     * @param {{position: ObservablePoint}} target anything that has a position
     */
    constructor(app, target) {
        super(app)

        app.ticker.add(() => {
            // Clamp the camera's movement between the extents of the player's movement and the window's width
            this.x = Math.max(
                0,
                Math.min(
                    target.position.x - 0.5 * VIRTUAL_WIDTH,
                    Math.min(MAP_WIDTH - VIRTUAL_WIDTH, target.position.x)
                )
            )

            app.stage.position.x = Math.floor(-this.x + 0.5)
        })
    }
}