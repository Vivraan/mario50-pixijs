import { Application, AnimatedSprite, Loader, LoaderResource, Texture } from 'pixi.js'
import { GameObject } from './game_object'
import { MAP_WIDTH, SPAWN_POINT, TILE_HEIGHT, TILE_WIDTH } from './tilemap'
import { deltatime, INPUT_MOVE_LEFT, INPUT_MOVE_RIGHT, INV_SCALE_FACTOR } from './utility'

// Dividing by the scale factor normalises the sprite's dimensions with respect to the viewport's.
const HEIGHT = 20 * INV_SCALE_FACTOR
const MOVE_SPEED = 120

/** The parameters to be passed to the Pixi application's loader. */
export const LOAD_PARAMS = ['player', '/graphics/blue_alien.json']

export class Player extends GameObject {
    /** @type {AnimatedSprite} */ _anim
    /** @type {{IDLE: Function, WALKING: Function, 'IN_AIR': Function}} */ _behaviour
    /** @type {string} */ _animState = 'IDLE'
    _vx = 0
    _vy = 0

    get position() { return this._anim.position }

    /**
     * The player character.
     * @param {Application} app
     */
    constructor(app) { super(app) }

    /**
     * The function consumed by the application's loader.
     * @param {Loader} _loader
     * @param {Partial<Record<string, LoaderResource>} resources
     */
    load(_loader, resources) {
        /** @type {{idle: Texture[], walking: Texture[], jumping: Texture[]}} */
        const animations = resources.player.spritesheet.animations
        console.log(animations.walking.length)

        this._anim = new AnimatedSprite(animations.idle)
        this._anim.loop = true
        this._anim.anchor.set(0.5)
        this._anim.position.set(
            SPAWN_POINT.x * TILE_WIDTH,
            SPAWN_POINT.y * TILE_HEIGHT + HEIGHT)

        this._app.stage.addChild(this._anim)

        // From https://jsfiddle.net/bigtimebuddy/qo12kdnj/
        let isLeft = false
        let isRight = false

        const onKeypress = ({ type, key }) => {
            const isKeyDown = type === 'keydown'
            switch (key) {
                case INPUT_MOVE_LEFT:
                    isLeft = isKeyDown
                    break
                case INPUT_MOVE_RIGHT:
                    isRight = isKeyDown
                    break
            }
        }

        window.addEventListener('keydown', onKeypress)
        window.addEventListener('keyup', onKeypress)

        let lastDirection = -1

        const move = () => {
            let direction = Number(isRight) - Number(isLeft)

            if (direction !== lastDirection) {
                if (direction === 0) {
                    this._animState = 'IDLE'
                    this._anim.textures = animations.idle
                    this._anim.scale.x = this._anim.scale.y
                } else {
                    this._animState = 'WALKING'
                    this._anim.textures = animations.walking
                    this._anim.scale.x = this._anim.scale.y * direction
                }
                this._anim.play()
                lastDirection = direction
            }
        }

        this._behaviour = Object.freeze({
            IDLE: move,
            WALKING: move,
            IN_AIR: () => { }
        })

        this._app.ticker.add(() => {
            const dt = deltatime(this._app.ticker)
            this._behaviour[this._animState].call()
            this._anim.update(dt)

            let x = this._anim.position.x
            x += this._vx * dt
            x = Math.max(0, Math.min(x, MAP_WIDTH))
            this._anim.position.x = x
        })
    }

    cleanup() {

    }
}
