import { Application } from "pixi.js"

/** @type {GameObject[]} */ const OBJECTS = []

export class GameObject {
    /** @type {Application} */ _app

    /**
     * An object that is registered with an app.
     * @param {Application} app
     */
    constructor(app) {
        this._app = app
        OBJECTS.push(this)
    }

    cleanup() { }

    static cleanupObjects() {
        OBJECTS.forEach(object => object.cleanup())
    }
}
