import { Application, Loader, Sprite, Texture } from 'pixi.js'
import { GameObject } from './game_object'

export const NX = 30
export const NY = 28

/** The player's spawn point. */
export const SPAWN_POINT = { x: 10, y: 0.5 * NY - 1 }
/** The width of a tile in the tilemap in pixels. */
export const TILE_WIDTH = 16
/** The height of a tile in the tilemap in pixels. */
export const TILE_HEIGHT = 16
/** The width of a tilemap in pixels. */
export const MAP_WIDTH = NX * TILE_WIDTH
/** The height of a tilemap in pixels. */
export const MAP_HEIGHT = NY * TILE_HEIGHT
/** The parameters to be passed to the Pixi application's loader. */
export const LOAD_PARAMS = ['spritesheet', '/graphics/spritesheet.json']

export class Tilemap extends GameObject {
    /** @type {Map<number, Texture>} */ tiles = new Map()

    /**
     * Retrieve grid coordinate as linear index.
     * @private
     * @param {number} x
     * @param {number} y
     */
    gridCoord_(x, y) {
        return y * NX + x
    }

    /**
     * @param {number} x grid coordinate x
     * @param {number} y grid coordinate y
     * @returns {Texture} texture of tile
     */
    getTile(x, y) {
        return this.tiles.get(this.gridCoord_(x, y))
    }

    /**
     * @param {Texture} tile texture to set
     * @param {number} x grid coordinate x
     * @param {number} y grid coordinate y
     */
    setTile(tile, x, y) {
        this.tiles.set(this.gridCoord_(x, y), tile)
    }

    /**
     * The tilemap representing the level's environment.
     * @param {Application} app
     */
    constructor(app) {
        super(app)
    }

    /**
     * The function consumed by the application's loader.
     * @param {Loader} _loader
     * @param {Partial<Record<string, LoaderResource>} resources
     */
    load(_loader, resources) {
        this.generateLevel(resources.spritesheet.textures)

        // Add tile to map at given grid coordinate
        for (let y = 0; y < NY; y++) {
            for (let x = 0; x < NX; x++) {
                const tile = this.getTile(x, y)
                if (tile !== undefined) {
                    const sprite = new Sprite(tile)
                    sprite.position.set(x * TILE_WIDTH, y * TILE_HEIGHT)
                    this._app.stage.addChild(sprite)
                }
            }
        }

        // this._app.ticker.add(() => {})
    }

    /**
     * Procedurally generate a playable level with given textures.
     * @param {ITextureDictionary} tiles The textures to use for setting up the tilemap
     */
    generateLevel(tiles) {
        for (let x = 0; x < NX;) {
            /**
             * Fill bricks below the specified ground level and advances to the next position.
             * @param {number} groundLevel The level from which bricks are placed vertically downwards
             */
            const advance = (groundLevel) => {
                groundLevel = groundLevel ?? 0
                for (let y = 0.5 * NY - groundLevel; y < NY; y++) {
                    this.setTile(tiles.brick, x, y)
                }
                // Move to the next tile's spot
                x++
            }

            const placeFlag = () => {
                this.setTile(tiles.flagpoleTop, x, 0.5 * NY - 4)
                // TODO place flag
                this.setTile(tiles.flagpoleMiddle, x, 0.5 * NY - 3)
                this.setTile(tiles.flagpoleMiddle, x, 0.5 * NY - 2)
                this.setTile(tiles.flagpoleBottom, x, 0.5 * NY - 1)

                advance()
            }

            const placePyramid = () => {
                advance()
            }

            const placeCloud = () => {
                if (x < NX - 2) {
                    /**
                     * Returns a random int between 0 and x.
                     * @param {number} x
                     */
                    function randomInt(x) {
                        Math.floor(Math.random() * Math.floor(x))
                    }

                    const cloudY = randomInt(0.5 * NY - 6)

                    if (this.getTile(x + 1, cloudY) != tiles.cloudRight) {
                        this.setTile(tiles.cloudLeft, x, cloudY)
                    }
                    this.setTile(tiles.cloudRight, x + 1, cloudY)
                }
            }

            const placeColumn = () => {
                this.setTile(tiles.columnTop, x, 0.5 * NY - 2)
                this.setTile(tiles.columnBottom, x, 0.5 * NY - 1)
                advance()
            }

            const placeBush = () => {
                // Make sure there's a gap left after placing bush tiles
                if (x < NX - 3) {
                    const bushY = 0.5 * NY - 1

                    this.setTile(tiles.bushLeft, x, bushY)
                    advance()
                    this.setTile(tiles.bushRight, x, bushY)
                    advance()
                }
            }

            const placeGround = () => {
                // 1 in 15 chance to generate a jump block
                if (Math.random() <= 1 / 15) {
                    this.setTile(tiles.jumpBlockActive, x, 0.5 * NY - 4)
                }

                advance()
            }

            // 5% independent chance for clouds!
            if (Math.random() <= 5 / 100) {
                placeCloud()
            }

            // Mandatory pyramid-and-flag close to the end of the level
            if (x >= NX - 10) {
                placePyramid()
            } else {
                const d = Math.random()
                if (d <= 5 / 100) { // 5% chance for a column
                    placeColumn()
                } else if (d <= (5 + 25) / 100) { // 25% chance for a bush
                    placeBush()
                } else if (d <= (10 + 25 + 60) / 100) { // 60% chance to not place any obstacle
                    placeGround()
                } else { // 10% chance for a 2 tile gap
                    x += 2
                }
            }

            if (x == SPAWN_POINT.x) {
                advance()
            }
        }
    }

    cleanup() {

    }
}
